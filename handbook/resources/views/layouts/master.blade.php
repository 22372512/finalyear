<!--Master document, containing the base HTML with inputs from the nav and footer and relative page.-->
<html>
<html lang="en">
<head>
<title>ModuleHandook</title>
<!--Links in the css for that page and using yeild to allow each page to have a specific title -->
<link rel="stylesheet" type="text/css" href="{{url('css/css.css')}}" />
<title>@yield('title')</title>
</head>
<body>
<div class="wrapper">
<!--Includes for the navigation -->
@include(' /layouts/nav')
<!-- Yeild inserts the relative pages into this master file -->
@yield('page')
<!--The footer of the page -->
@include('/layouts/foot')
</div>
</body>
</html>