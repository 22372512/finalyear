<!--Contains a home button to go back to the home page, and a logout button to logout.-->
<!--Using laravels auth check too see if the user is logged in. If not he wont see tha nav bar. -->
@if (Auth::check())
<nav>
<a href="/admins/home">Home</a>
<a href="/auth/logout">Logout</a>
</nav>
@endif