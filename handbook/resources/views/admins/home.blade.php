<!--Current place holder home page to check that everything works.
Will contain the relative items for the different authors on a later date.
Extends the master page with a title
Has different levels for different users depending on their rank in the DB,
So they can only view the contain made for them -->
@extends('layouts.master')
@section('title')
    Home Page
@stop

@section('page')
    <h1>HomePage</h1>
    
    <!--using the rank in the database, allows different ranks of people to see different items -->
    <?php $level = (Auth::user()->rank); ?>
    @if ($level == '1')
    <div class="welcome">
        <p>U logged in as master admin </p>
    </div>
    @elseif ($level == '2')
    <div class="welcome">
        <p> U logged in as a module leader</p>
    </div>
    @elseif ($level == '3')
    <div class="welcome">
        <p> U logged in as a student</p>
    </div>

    @endif

@stop