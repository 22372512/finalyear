<!-- Login Form that will allow users to login
Extends master so it will be inputed in to the master file
Has a title for the page
Uses a csrf field to secure the page from spam login attacks
contains laravels default login form
-->

@extends('layouts.master')
@section('title')
     Login - MH
@stop
@section ('page')
<div class="login">
<form method="post" action="/auth/login">
{!! csrf_field() !!}
<div class="log">
    <input type="email" name="email" value="{{ old('email') }}"/>
</div>

<div class="log">
    <input type="password" name="password" id="password" placeholder="Password" />
</div>

<div class="log">
    <input type="checkbox" name="remember"> Remember Me
</div>

<div class="log">
    <button type="submit">Login</button>
</div>

</form>
</div>
@stop