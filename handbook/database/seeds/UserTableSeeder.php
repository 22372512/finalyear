<!-- DB Seeder, populates the most important information in to the users table
Imports 3 people, and admin a module leader and a student-->
<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Clear db - Could be used with factories - but to stop the db from being cluttered wont be used
        //DB::table('users')->truncate();

        //Inserting an admin into the table
        DB::table('users')->insert([
        	'name' => 'Betty',
        	'email' => 'betty@test.com',
        	'password' => bcrypt('test'),
        	'rank' => 1
        ]);

        DB::table('users')->insert([
          'name' => 'Bob',
          'email' => 'bob@test.com',
          'password' => bcrypt('test1'),
          'rank' => 2
        ]);

        DB::table('users')->insert([
          'name' => 'Bill',
          'email' => 'bill@test.com',
          'password' => bcrypt('test2'),
          'rank' => 3
        ]);
    }
}
